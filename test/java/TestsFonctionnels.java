import fr.ufc.l3info.oprog.*;
import fr.ufc.l3info.oprog.parser.StationParser;
import fr.ufc.l3info.oprog.parser.StationParserException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class TestsFonctionnels {

    /** Chemin vers les fichiers de test */
    final String path = "./target/classes/data/";

    /** Instance singleton du parser de stations */
    final StationParser parser = StationParser.getInstance();

    @Test
    public void testVilleStation() throws IOException, StationParserException {
        Ville v = new Ville();
        v.initialiser(new File("./target/classes/data/stationsOK.txt"));
        Abonne a = v.creerAbonne("Fred", "00000-00000-00000000000-97");
        Assert.assertNotNull(v.getStation("21 - Avenue Fontaine Argent, Boulevard Diderot"));
    }

    @Test
    public void testVilleAbonne() throws IOException, StationParserException {
        Ville v = new Ville();
        v.initialiser(new File("./target/classes/data/stationsOK.txt"));
        Abonne a = v.creerAbonne("Fred", "00000-00000-00000000000-97");
        Assert.assertEquals("Fred", a.getNom());
    }

    @Test
    public void testExploitant() throws IOException, StationParserException {
        Ville v = new Ville();
        v.initialiser(new File("./target/classes/data/stationsOK.txt"));
        FabriqueVelo fab = FabriqueVelo.getInstance();
        IVelo v1 = fab.construire('H',"");
        IVelo v2 = fab.construire('H',"");
        IVelo v3 = fab.construire('H',"");
        Exploitant ex = new Exploitant();
        ex.acquerirVelo(v1);
        ex.acquerirVelo(v2);
        Abonne a = v.creerAbonne("Fred", "00000-00000-00000000000-97");
        //System.out.println(v.getStation("21 - Avenue Fontaine Argent, Boulevard Diderot").arrimerVelo(v3, 1));
        //v.getStation("21 - Avenue Fontaine Argent, Boulevard Diderot").veloALaBorne(1).abimer();
        ex.ravitailler(v);
        Assert.assertEquals(v1,v.getStation("21 - Avenue Fontaine Argent, Boulevard Diderot").veloALaBorne(1));
    }

}
