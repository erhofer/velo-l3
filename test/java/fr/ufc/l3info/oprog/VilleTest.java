package fr.ufc.l3info.oprog;
import fr.ufc.l3info.oprog.parser.ASTNode;
import fr.ufc.l3info.oprog.parser.ASTStationBuilder;
import fr.ufc.l3info.oprog.parser.StationParser;
import fr.ufc.l3info.oprog.parser.StationParserException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class VilleTest {

    /** Chemin vers les fichiers de test */
    final String path = "./target/classes/data/";

    /** Instance singleton du parser de stations */
    final StationParser parser = StationParser.getInstance();

    @Test
    public void testinitialiser() throws IOException, StationParserException {
        Ville v = new Ville();
        v.initialiser(new File("./target/classes/data/stationsOK.txt"));
        Assert.assertNotNull(v.getStation("21 - Avenue Fontaine Argent, Boulevard Diderot"));
    }

    @Test
    public void testinitialiserFail() throws IOException, StationParserException {
        Ville v = new Ville();
        v.initialiser(new File("./target/classes/data/stationsOK.txt"));
        Assert.assertNull(v.getStation("21 - Avenue Jean Bertrand, Boulevard Diderot"));
    }

    @Test
    public void testStationPlusProche() throws IOException, StationParserException {
        Ville v = new Ville();
        v.initialiser(new File("./target/classes/data/stationsOK.txt"));
        Assert.assertEquals(v.getStation("21 - Avenue Fontaine Argent, Boulevard Diderot"),v.getStationPlusProche(47.5,2));
    }

    @Test
    public void testStationNotProche() throws IOException, StationParserException {
        Ville v = new Ville();
        v.initialiser(new File("./target/classes/data/stationsOK.txt"));
        Assert.assertNotEquals(v.getStation("Avenue du Marechal Foch"),v.getStationPlusProche(47.5,2));
    }

    @Test
    public void testStationNotProcheecauseNull() throws IOException, StationParserException {
        Ville v = new Ville();
        v.initialiser(new File("./target/classes/data/stationsOK.txt"));
        Assert.assertNotEquals(v.getStation("Avenue du Mec moche"),v.getStationPlusProche(47.5,2));
    }

    @Test
    public void testCreerAbonne() throws IOException, StationParserException {
        Ville v = new Ville();
        v.initialiser(new File("./target/classes/data/stationsOK.txt"));
        Abonne a = v.creerAbonne("Fred", "00000-00000-00000000000-97");
        Assert.assertEquals("Fred", a.getNom());
    }

    @Test//(expected = IncorrectNameException.class)
    public void testCreerAbonneNullName() throws IOException, StationParserException {
        Ville v = new Ville();
        v.initialiser(new File("./target/classes/data/stationsOK.txt"));
        Abonne a = v.creerAbonne("F r e --d", "00000-00000-00000000000-97");
        Assert.assertNull(a);
    }

    /*@Test
    public void testIterator() throws IOException, StationParserException {
        Ville v = new Ville();
        v.initialiser(new File("./target/classes/data/stationsOK.txt"));
        ASTNode n = parser.parse(new File("./target/classes/data/stationsOK.txt"));
        ASTStationBuilder builder = new ASTStationBuilder();
        n.accept(builder);
        ClosestStationIterator closestStationIterator = new ClosestStationIterator(builder.getStations(), builder.getStations().iterator().next());

        Assert.assertEquals(closestStationIterator. v.iterator().next());
    }*/
}
